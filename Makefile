# Defaults
BROKER=0.0.0.0:19092
TOPIC=test
BUCKET=streaming

all-up:	dagster-up kafka-up s3-up

all-down: dagster-down kafka-down s3-down

# Dagster
dagster-up:
	docker compose --profile dagster up -d --build

dagster-down:
	docker compose --profile dagster down -v --remove-orphans

# Kafka
kafka-up:
	docker compose --profile kafka up -d --build

kafka-down:
	docker compose --profile kafka down -v --remove-orphans

kafka-list:
	kcat -b $(BROKER) -L

kafka-topic:
	kcat -b $(BROKER) -t $(TOPIC) -f 'Topic %t[%p], timestamp: %T, offset: %o, key: %k, payload: %S bytes: %s\n'

# s3
s3-up:
	docker compose --profile s3 up -d

s3-down:
	docker compose --profile s3 down -v --remove-orphans

s3-mb:
	aws --endpoint-url=http://0.0.0.0:4566 s3 mb s3://$(BUCKET)

s3-ls:
	aws --endpoint-url=http://0.0.0.0:4566 s3 ls $(PATH)

pyspark:
	pyspark \
	  --conf spark.hadoop.fs.s3a.endpoint=http://localhost:4566 \
	  --conf spark.hadoop.fs.s3a.impl=org.apache.hadoop.fs.s3a.S3AFileSystem \
	  --conf spark.hadoop.fs.s3a.access.key=foo \
	  --conf spark.hadoop.fs.s3a.secret.key=bar \
	  --conf spark.hadoop.fs.s3a.path.style.access=true
