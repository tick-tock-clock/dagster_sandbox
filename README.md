# DataOps ETL Sandbox

Dockerized sandbox environment to get started with the following:
- Python3.10
- Orchestration / ETL
  - [Dagit](https://docs.dagster.io/concepts/dagit/dagit#dagit-ui)
  - [Dagster Daemon](https://docs.dagster.io/deployment/dagster-daemon#dagster-daemon)
  - [User code base](https://docs.dagster.io/tutorial/advanced-tutorial/repositories#advanced-organizing-jobs-in-repositories--workspaces)
  - [Postgres](https://hub.docker.com/_/postgres)
- Streaming
  - [Zookeeper](https://hub.docker.com/r/confluentinc/cp-zookeeper)
  - [Kafka Brokers](https://hub.docker.com/r/confluentinc/cp-kafka)
  - [Schema Registry](https://hub.docker.com/r/confluentinc/cp-schema-registry)
  - [Schema Registry UI](https://registry.hub.docker.com/r/landoop/schema-registry-ui)
  - [Kafka Connect S3 Sink](https://www.confluent.io/hub/confluentinc/kafka-connect-s3)
  - [Apache PySpark](https://spark.apache.org/docs/latest/api/python/index.html)
- Storage
  - [local S3 with localstack](https://registry.hub.docker.com/r/localstack/localstack)
    
## Dagster/Kafka Workflow
See [Makefile](Makefile) for possible actions.

## Local Development

```bash
# install dependencies for development
./pip_install
```

## Learnings

### Apple M1 and confluent-kafka

To ensure compatibility and avoid errors (as of 2022-03-13) you have to do the following before installing
confluent-kafka (See [Issue](https://segmentfault.com/a/1190000040867082/en)):

```shell
C_INCLUDE_PATH=/opt/homebrew/Cellar/librdkafka/1.8.2/include \
LIBRARY_PATH=/opt/homebrew/Cellar/librdkafka/1.8.2/lib \
./pip_install
```

### Docker and confluent-kafka:
```dockerfile
RUN git clone https://github.com/edenhill/librdkafka.git
RUN cd librdkafka && ./configure && make && make install && ldconfig
RUN pip install confluent-kafka[avro]==1.8.2
```

### PySpark 3.2.1 on Mac

```bash
# Install pyspark with pip
pip install pyspark==3.2.1

# Install java with brew
brew install openjdk@11

# For the system Java wrappers to find this JDK, symlink it with
susudo ln -sfn /opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk

# If you need to have openjdk@11 first in your PATH, run:
echo 'export PATH="/opt/homebrew/opt/openjdk@11/bin:$PATH"' >> ~/.zshrc

# For compilers to find openjdk@11 you may need to set:
export CPPFLAGS="-I/opt/homebrew/opt/openjdk@11/include"
```

Add the following to `venv/lib/python3.10/site-packages/pyspark/jars`.

Can be downloaded and put into repcetive directory by running [scripts/download_pyspark_dependencies.py](scripts/download_pyspark_dependencies.py).
#### Requirements
- Python3.10
- Pyspark installed in virtual environment like [virtualenv](https://formulae.brew.sh/formula/virtualenv)
- https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-bundle/1.11.901/aws-java-sdk-bundle-1.11.901.jar
- https://repo1.maven.org/maven2/io/delta/delta-core_2.12/1.1.0/delta-core_2.12-1.1.0.jar
- https://repo1.maven.org/maven2/org/apache/commons/commons-pool2/2.6.2/commons-pool2-2.6.2.jar
- https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.3.1/hadoop-aws-3.3.1.jar
- https://repo1.maven.org/maven2/org/apache/kafka/kafka-clients/2.8.0/kafka-clients-2.8.0.jar
- https://repo1.maven.org/maven2/org/apache/spark/spark-avro_2.13/3.2.1/spark-avro_2.13-3.2.1.jar
- https://repo1.maven.org/maven2/org/apache/spark/spark-sql-kafka-0-10_2.12/3.2.1/spark-sql-kafka-0-10_2.12-3.2.1.jar
- https://repo1.maven.org/maven2/org/apache/spark/spark-token-provider-kafka-0-10_2.12/3.2.1/spark-token-provider-kafka-0-10_2.12-3.2.1.jar
