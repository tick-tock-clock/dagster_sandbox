from datetime import datetime

from dagster import job, op, DynamicOut, Field, DynamicOutput, Array, OpExecutionContext
from dateutil.tz import tz

run_config = {
    'ops': {
        'get_time_op': {
            'config': {
                'timezones': ['UTC', 'Europe/Berlin', 'America/Chicago']
            }
        }
    }
}


def get_time(timezone: str) -> str:
    return str(datetime.now(tz=tz.gettz(timezone)))


@op(config_schema={'timezones': Field(Array(str))},
    out=DynamicOut(str))
def get_time_op(context):
    for _tz in context.op_config['timezones']:
        yield DynamicOutput(get_time(_tz), mapping_key=str(_tz).split('/')[-1])


@op
def print_time(context: OpExecutionContext, time: str) -> None:
    context.log.info(f'{context.get_mapping_key()} time: {time}')


@job(config=run_config)
def print_time_job():
    get_time_op().map(print_time)
