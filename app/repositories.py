from dagster import repository

from jobs.global_timezones import print_time_job


@repository
def batch_repository():
    return [print_time_job]
