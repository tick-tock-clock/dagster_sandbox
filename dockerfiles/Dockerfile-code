FROM python:3.10-slim-buster as dagster-setup

ARG DAGSTER_VERSION

ENV DAGSTER_HOME=/opt/dagster/dagster_home/
RUN mkdir -p $DAGSTER_HOME

RUN apt update && apt install -y git g++ make

RUN pip install --upgrade pip && \
    pip install \
    dagster-docker==$DAGSTER_VERSION \
    dagster-postgres==$DAGSTER_VERSION \
    psycopg2-binary==2.9.3 \
    dagster==$DAGSTER_VERSION

# Confluent Kafka setup
RUN git clone https://github.com/edenhill/librdkafka.git
RUN cd librdkafka && ./configure && make && make install && ldconfig
RUN pip install confluent-kafka[avro]==1.8.2
RUN apt remove -y && apt autoremove -y


FROM dagster-setup

COPY ../app /opt/dagster/app

WORKDIR /opt/dagster/app

EXPOSE 4000

CMD ["dagster", "api", "grpc", "-h", "0.0.0.0", "-p", "4000", "-f", "repositories.py"]