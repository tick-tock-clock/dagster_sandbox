set -e

install() {

  read -p 'Input Dagster version or leave blank to read from .env file or default to 0.14.6: ' DAGSTER_VERSION
  local DAGSTER_VERSION=$DAGSTER_VERSION

  if [ -z ${DAGSTER_VERSION} ]; then
    if [ -f .env ]; then
      echo 'Getting DAGSTER_VERSION from .env file...'
      export $(grep -v '^#' .env | xargs)

    elif [ -z ${DAGSTER_VERSION} ]; then
      local DAGSTER_VERSION=0.14.6
    fi
  fi

  pip3 install --upgrade $@ $(sed s/DAGSTER_VERSION/$DAGSTER_VERSION/ requirements.txt)
}

install
