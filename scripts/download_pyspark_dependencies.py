import os.path
from urllib import request

from pyspark import __file__ as pyspark_path

JAR_URLS = (
    'https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-bundle/1.11.901/aws-java-sdk-bundle-1.11.901.jar',
    'https://repo1.maven.org/maven2/io/delta/delta-core_2.12/1.1.0/delta-core_2.12-1.1.0.jar',
    'https://repo1.maven.org/maven2/org/apache/commons/commons-pool2/2.6.2/commons-pool2-2.6.2.jar',
    'https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.3.1/hadoop-aws-3.3.1.jar',
    'https://repo1.maven.org/maven2/org/apache/kafka/kafka-clients/2.8.0/kafka-clients-2.8.0.jar',
    'https://repo1.maven.org/maven2/org/apache/spark/spark-avro_2.13/3.2.1/spark-avro_2.13-3.2.1.jar',
    'https://repo1.maven.org/maven2/org/apache/spark/spark-sql-kafka-0-10_2.12/3.2.1/spark-sql-kafka-0-10_2.12-3.2.1.jar',
    'https://repo1.maven.org/maven2/org/apache/spark/spark-token-provider-kafka-0-10_2.12/3.2.1/spark-token-provider-kafka-0-10_2.12-3.2.1.jar',
)

if __name__ == '__main__':
    for url in JAR_URLS:
        jar_name = url.split('/')[-1]
        jars_path = pyspark_path.replace('__init__.py', 'jars')
        output_path = os.path.join(jars_path, jar_name)
        if not os.path.exists(output_path):
            print(f'Downloading {jar_name} into {output_path}')
            request.urlretrieve(url, output_path)
        else:
            print(f'{jar_name} already in {jars_path}')
