from sys import argv
from time import sleep

import requests

"""
TODO: partition data

Connector Documentation
https://docs.confluent.io/5.4.2/connect/kafka-connect-s3/index.html
"""

HEADERS = {'Accept': 'application/json'}
ENDPOINT = 'http://0.0.0.0:8083/connectors/sink-s3/config'
CONFIG = {
    "connector.class": "io.confluent.connect.s3.S3SinkConnector",
    "key.converter": "io.confluent.connect.avro.AvroConverter",
    "value.converter": "io.confluent.connect.avro.AvroConverter",
    "key.converter.schema.registry.url": "http://schema-registry:8081",
    "value.converter.schema.registry.url": "http://schema-registry:8081",
    "s3.region": "eu-central-1",
    "s3.part.size": "5242880",
    "store.url": "http://s3:4566",
    "flush.size": "65536",
    "timezone": 'UTC',
    "storage.class": "io.confluent.connect.s3.storage.S3Storage",
    "format.class": "io.confluent.connect.s3.format.avro.AvroFormat",
    "schema.generator.class": "io.confluent.connect.storage.hive.schema.DefaultSchemaGenerator",
    "schema.compatibility": "NONE",
    "errors.log.enable": "true",
    "errors.log.include.messages": "true",
    "partitioner.class": "io.confluent.connect.storage.partitioner.DefaultPartitioner"
}


def configure(topic: str, s3_bucket: str, processing_time_ms: str) -> None:
    config = CONFIG | {"topics": topic,
                       "s3.bucket.name": s3_bucket,
                       "rotate.schedule.interval.ms": processing_time_ms}
    res = requests.put(url=ENDPOINT, json=config, headers=HEADERS).json()
    print(res)


def info() -> None:
    res = requests.get('http://0.0.0.0:8083/connectors?expand=info').json()
    print(res)


def status() -> None:
    print('Waiting 10 sec for connector setup to be setup finished...')
    sleep(10)
    res = requests.get('http://0.0.0.0:8083/connectors?expand=status').json()
    if res.get('sink-s3'):
        if res['sink-s3']['status']['connector']['state'] == 'RUNNING':
            print('Connector is running!')
        else:
            print(res)
    else:
        status()


def run(topic: str, s3_bucket: str, processing_time_ms: str):
    try:
        configure(topic, s3_bucket, processing_time_ms)
        info()
        status()
    except requests.exceptions.ConnectionError as e:
        print(f'Kafka-Connect not ready yet. {e.args[0]}\n Retrying in 10 seconds...')
        sleep(10)
        run(topic, s3_bucket, processing_time_ms)


if __name__ == '__main__':
    if len(argv) != 4:
        print('''
           Program arguments: <topic> <s3_bucket> <processing_time_ms>
           ''')
        exit(-1)
    run(argv[1], argv[2], argv[3])
