from sys import argv, exit
from typing import Dict, Any, List

from confluent_kafka.admin import AdminClient
from confluent_kafka.cimpl import NewTopic

if __name__ == '__main__':
    if len(argv) != 3:
        print('''
        Program arguments: <bootstrap.servers: str (comma separated)> <topics:str (comma separated)>
        ''')
        exit(-1)

    bootstrap_servers: str = argv[1]
    topic_names: List[str] = argv[2].split(',')
    admin = AdminClient({'bootstrap.servers': bootstrap_servers})

    new_topics = [NewTopic(topic, num_partitions=3, replication_factor=1) for topic in topic_names]

    # Call create_topics to asynchronously create topics. A dict
    # of <topic,future> is returned.
    fs: Dict[str, Any] = admin.create_topics(new_topics)

    # Wait for each operation to finish.
    for topic, f in fs.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} created".format(topic))
        except KeyboardInterrupt:
            print("Program terminated!")
        except Exception as e:
            print("Failed to create topic {}: {}".format(topic, e))
