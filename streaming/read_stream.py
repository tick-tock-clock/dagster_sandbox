from sys import argv

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import current_timestamp, col, lpad, from_unixtime


class NewSparkSession:
    def __init__(self, **kwargs):
        self.app_name = kwargs.get('app_name', 'spark-app')
        self.master_url = kwargs.get('master_url', 'local[*]')

        self.aws_access_key = kwargs.get('aws_access_key', 'foo')
        self.aws_secret_key = kwargs.get('aws_secret_key', 'bar')
        self.aws_endpoint_url = kwargs.get('aws_endpoint_url', 'http://0.0.0.0:4566')

    def get_or_create(self) -> SparkSession:
        conf = SparkConf().setAppName(self.app_name)
        conf.set('fs.s3a.endpoint', self.aws_endpoint_url)
        conf.set('fs.s3a.access.key', self.aws_access_key)
        conf.set('fs.s3a.secret.key', self.aws_secret_key)
        conf.set('spark.sql.streaming.schemaInference', 'true')
        conf.set('fs.s3a.impl', 'org.apache.hadoop.fs.s3a.S3AFileSystem')
        return (SparkSession
                .builder
                .master(self.master_url)
                .config(conf=SparkContext(conf=conf).getConf())
                .getOrCreate())


if __name__ == '__main__':
    if len(argv) != 3:
        print('''
           Program arguments: <topic> <processing_time (e.g. "5 seconds", "1 minute")>
           ''')
        exit(-1)
    topic: str = argv[1]
    processing_time: str = argv[2]
    spark = NewSparkSession().get_or_create()
    df = (spark
          .readStream
          .format('avro')
          .load(f's3a://streaming/topics/{topic}'))

    df = (df
          .withColumn('dt', from_unixtime(col('event_timestamp'), 'yyyy-MM-dd'))
          .withColumn('hr', lpad(from_unixtime(col('event_timestamp'), 'HH'), 2, "0"))
          .withColumn('min', lpad(from_unixtime(col('event_timestamp'), 'mm'), 2, "0"))
          .withColumn('processed', current_timestamp()))

    stream_query = (df
                    .writeStream
                    .format('delta')
                    .partitionBy(['dt', 'hr', 'min'])
                    .option('checkpointLocation', f's3a://bronze-tables/{topic}/_checkpoint')
                    .trigger(processingTime=processing_time)
                    .start(f's3a://bronze-tables/{topic}'))

    stream_query.awaitTermination()
